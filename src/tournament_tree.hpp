#include <memory>
#include <unordered_map>
#include <utility>
#include <vector>

template <class T> struct TournamentTreeNode {
  TournamentTreeNode<T> *parent = nullptr;
  std::unique_ptr<TournamentTreeNode<T>> left = nullptr, right = nullptr;
  T *value;

  TournamentTreeNode(T *value) : value(value) {}
};

template <class T> class TournamentTree {
private:
  std::unordered_map<T *, TournamentTreeNode<T> *> leave_by_queue;
  std::unique_ptr<TournamentTreeNode<T>> root;

  void build_tree(std::vector<std::unique_ptr<TournamentTreeNode<T>>> &nodes) {
    if (nodes.empty())
      return;
    if (nodes.size() == 1) {
      this->root = std::move(nodes.front());
      return;
    }

    std::vector<std::unique_ptr<TournamentTreeNode<T>>> next;

    std::unique_ptr<TournamentTreeNode<T>> parent;
    while (!nodes.empty()) {
      std::unique_ptr<TournamentTreeNode<T>> left = std::move(nodes.front());
      nodes.erase(nodes.begin());

      std::unique_ptr<TournamentTreeNode<T>> right = nullptr;
      if (!nodes.empty()) {
        right = std::move(nodes.front());
        nodes.erase(nodes.begin());
      }

      if (!right || left->value->front() < right->value->front()) {
        parent = std::make_unique<TournamentTreeNode<T>>(left->value);
      } else {
        parent = std::make_unique<TournamentTreeNode<T>>(right->value);
      }

      left->parent = parent.get();
      parent->left = std::move(left);

      if (right) {
        right->parent = parent.get();
        parent->right = std::move(right);
      }
      next.push_back(std::move(parent));
    }

    this->build_tree(next);
  }

  void update(TournamentTreeNode<T> *parent) {
    if (!parent)
      return;
    else if (!parent->right) {
      parent->value = parent->left->value;
    } else if (!parent->left) {
      parent->value = parent->right->value;
    } else if (parent->right->value->empty()) {
      parent->value = parent->left->value;
    } else if (parent->left->value->empty()) {
      parent->value = parent->right->value;
    } else if (parent->left->value->front() < parent->right->value->front()) {
      parent->value = parent->left->value;
    } else {
      parent->value = parent->right->value;
    }

    this->update(parent->parent);
  }

public:
  // XXX: Use initializer list
  TournamentTree(const std::vector<T *> &heads) : root(nullptr) {
    std::vector<std::unique_ptr<TournamentTreeNode<T>>> next;
    for (const auto head : heads) {
      auto leave(std::make_unique<TournamentTreeNode<T>>(head));
      this->leave_by_queue.insert(std::make_pair(head, leave.get()));
      next.push_back(std::move(leave));
    }

    this->build_tree(next);
  }

  auto front() const { return this->root->value->front(); }

  void pop() {
    this->root->value->pop();

    auto parent = this->leave_by_queue[this->root->value]->parent;
    this->update(parent);
  }

  bool empty() const {
    if (!this->root)
      return true;
    else
      return this->root->value->empty();
  }
};
