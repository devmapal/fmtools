#pragma once

#include "function2/function2.hpp"
#include <optional>
#include <queue>
#include <shared_mutex>

template <class T> class MinSizeQueue {
private:
  std::queue<T> queue;
  size_t min_size;
  fu2::unique_function<std::optional<T>()> load_next;
  std::shared_mutex queue_mutex;

  void grow() {
    while (true) {
      {
        std::shared_lock lock(queue_mutex);
        if (queue.size() < min_size)
          break;
      }

      auto next = load_next();
      if (next.has_value()) {
        std::unique_lock lock(queue_mutex);
        queue.push(next.value());
      } else {
        break;
      }
    }
  }

public:
  MinSizeQueue(fu2::unique_function<std::optional<T>()> &&load_next,
               size_t min_size = 0)
      : min_size(min_size), load_next(std::move(load_next)) {
    grow();
  }

  void push(const T &val) { queue.push(val); }

  void pop() {
    {
      std::unique_lock lock(queue_mutex);
      queue.pop();
    }
    grow();
  }

  const T &front() const {
    std::shared_lock lock(queue_mutex);
    return queue.front();
  }

  const T &back() const {
    std::shared_lock lock(queue_mutex);
    return queue.back();
  }

  size_t size() const {
    std::shared_lock lock(queue_mutex);
    return queue.size();
  }

  bool empty() const {
    std::shared_lock lock(queue_mutex);
    return queue.empty();
  }
};
