#include <string>

namespace mtools::regex {
std::string iso8601_re =
    "(\\d\\d\\d\\d)-?(\\d\\d)-?(\\d\\d)T(\\d\\d):?(\\d\\d):?"
    "(\\d\\d):?\\.(\\d\\d\\d)(?:(Z)|(?:[+-](\\d\\d):?(\\d\\d)?))?";
}
