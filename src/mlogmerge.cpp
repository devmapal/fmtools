#include "boost/date_time/local_time/local_time.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/locale.hpp"
#include "docopt/docopt.h"
#include "min_size_queue.hpp"
#include "mtools_config.hpp"
#include "regex.hpp"
#include "tournament_tree.hpp"
#include <fstream>
#include <iostream>
#include <istream>
#include <pcrecpp.h>
#include <stdexcept>
#include <string>
#include <vector>

static const char USAGE[] =
    R"(mlogmerge.

    Usage:
      mlogmerge [<logfile>...]

    Options:
      -h --help     Show this screen.test
      --version     Show version.
)";

pcrecpp::RE re(mtools::regex::iso8601_re + ".*");

struct LogLine {
  std::string line;
  std::unique_ptr<boost::local_time::local_date_time> timestamp = nullptr;

  LogLine(const LogLine &other) : line(other.line) {
    if (other.timestamp)
      this->timestamp =
          std::move(std::make_unique<boost::local_time::local_date_time>(
              *(other.timestamp)));
  }

  LogLine(const std::string &line) : line(line) {
    int year, month, day, hours, minutes, seconds, milliseconds;
    std::string tz_h, tz_m;
    if (re.FullMatch(line, &year, &month, &day, &hours, &minutes, &seconds,
                     &milliseconds, &tz_h, &tz_m)) {
      boost::posix_time::ptime pt(
          boost::gregorian::date(year, month, day),
          boost::posix_time::hours(hours) +
              boost::posix_time::minutes(minutes) +
              boost::posix_time::seconds(seconds) +
              boost::posix_time::milliseconds(milliseconds));
      std::string tz_str(boost::locale::time_zone::global());
      if (tz_str == "")
        tz_str = "Z";
      if (!tz_h.empty()) {
        tz_str = tz_h;

        if (!tz_m.empty())
          tz_str += ":" + tz_m;
      }

      boost::local_time::time_zone_ptr tz{
          new boost::local_time::posix_time_zone(tz_str)};

      timestamp = std::make_unique<boost::local_time::local_date_time>(pt, tz);
    } else {
      throw std::invalid_argument("unable to parse logline");
    }
  }

  bool operator<(const LogLine &rhs) const {
    return *(this->timestamp) < *(rhs.timestamp);
  }
};

std::optional<LogLine> read_line(std::istream &file) {
  if (!file.eof()) {
    std::string line;
    std::getline(file, line);
    try {
      return std::make_optional<LogLine>(line);
    } catch (const std::invalid_argument &error) {
    }
  }

  return std::nullopt;
}

std::unique_ptr<MinSizeQueue<LogLine>> read_file(const std::string &file) {
  std::ifstream file_stream(file, std::ifstream::in);
  auto fn = std::move(std::bind(read_line, std::move(file_stream)));
  std::unique_ptr<MinSizeQueue<LogLine>> queue(
      std::make_unique<MinSizeQueue<LogLine>>(std::move(fn), 2));
  return queue;
}

int main(int argc, const char **argv) {
  std::map<std::string, docopt::value> args =
      docopt::docopt(USAGE, {argv + 1, argv + argc}, true,
                     std::to_string(MTOOLS_VERSION_MAJOR) + "." +
                         std::to_string(MTOOLS_VERSION_MINOR));

  const auto &arg = args["<logfile>"].asStringList();
  std::vector<std::unique_ptr<MinSizeQueue<LogLine>>> queues;
  std::vector<MinSizeQueue<LogLine> *> heads;

  if (arg.size()) {
    for (const auto &file : arg) {
      queues.emplace_back(read_file(file));
      heads.emplace_back(queues.back().get());
    }
  } else {
    // queues.emplace_back(read_file(std::cin));
  }
  TournamentTree<MinSizeQueue<LogLine>> tree(heads);

  while (!tree.empty()) {
    std::cout << tree.front().line << std::endl;
    tree.pop();
  }

  return 0;
}
