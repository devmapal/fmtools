#include "../src/tournament_tree.hpp"
#include <catch2/catch.hpp>
#include <queue>
#include <vector>

SCENARIO("TournamentTrees return elements in the expected order",
         "[TournamentTree]") {
  GIVEN("An empty TournamentTree") {
    std::vector<std::queue<int> *> heads;
    TournamentTree<std::queue<int>> tree(heads);

    WHEN("Asking if the tree is empty") {
      bool is_empty = tree.empty();

      THEN("The tree should return true") { REQUIRE(is_empty == true); }
    }
  }

  GIVEN("A TournamentTree with non-empty queues") {
    auto queue_count = GENERATE(1, 2, 3, 4, 5, 8);
    auto same_length_queues = GENERATE(true, false);

    std::vector<std::unique_ptr<std::queue<int>>> raii_head;
    std::vector<std::queue<int> *> heads;
    for (int i = 0; i < queue_count; ++i) {
      auto queue(std::make_unique<std::queue<int>>());
      int element_count = (same_length_queues ? 2 : i) + 1;
      for (int val = 0; val < element_count; ++val) {
        queue->push(10 * i + val);
      }
      heads.push_back(queue.get());
      raii_head.push_back(std::move(queue));
    }
    TournamentTree<std::queue<int>> tree(heads);

    WHEN("Asking if the tree is empty") {
      bool is_empty = tree.empty();

      THEN("The tree should return false") { REQUIRE(is_empty == false); }
    }

    WHEN("Consuming the tree") {
      int last = std::numeric_limits<int>::min();

      THEN("It should return the elements in ascending order") {
        while (!tree.empty()) {
          int current = tree.front();
          tree.pop();

          REQUIRE(current > last);
          last = current;
        }
      }

      THEN("It should return all elements") {
        int expected = 0;
        for(const auto& head: heads) {
          expected += head->size();
        }

        int actual = 0;
        while (!tree.empty()) {
          tree.pop();
          ++actual;
        }
        REQUIRE(expected == actual);
      }
    }
  }
}
